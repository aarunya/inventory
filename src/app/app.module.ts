import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { AppComponent } from './app.component';
import { metaReducers, reducers } from './reducers';
import { SharedModule } from './shared/shared.module';
import { PageNotFoundComponent } from './shared/ui/page-not-found/page-not-found.component';
import { ShellModule } from './shell/shell.module';

const ROUTES: Routes = [
	{ path: '', pathMatch: 'full', redirectTo: '/my-inventory' },
	{
		path: 'my-inventory',
		loadChildren: './shell/shell.module#ShellModule',
	},
	{
		path: 'not-found',
		component: PageNotFoundComponent,
	},
	{
		path: '**',
		redirectTo: 'not-found',
	},
];
@NgModule({
	schemas: [
		CUSTOM_ELEMENTS_SCHEMA,
	],
	declarations: [
		AppComponent,
		PageNotFoundComponent,
	],
	imports: [
		RouterModule.forRoot(ROUTES),
		SharedModule,
		ShellModule,
		BrowserAnimationsModule,
		StoreModule.forRoot(reducers, {
			metaReducers,
			runtimeChecks: {
				strictStateImmutability: true,
				strictActionImmutability: true,
			},
		}),
	],
	providers: [],
	bootstrap: [AppComponent],
})
export class AppModule { }
