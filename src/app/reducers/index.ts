import {
	ActionReducer,
	ActionReducerMap,
	createFeatureSelector,
	createSelector,
	MetaReducer,
} from '@ngrx/store';
import { environment } from '../../environments/environment';

// tslint:disable-next-line: interface-name
export interface State {
	data?: any;
}

export const reducers: ActionReducerMap<State> = {

};

export const metaReducers: Array<MetaReducer<State>> = !environment.production ? [] : [];
