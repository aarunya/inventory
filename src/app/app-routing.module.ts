import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShellModule } from './shell/shell.module';

@NgModule({
	schemas: [
		CUSTOM_ELEMENTS_SCHEMA,
	],
	imports: [
		ShellModule],
	exports: [RouterModule],
})
export class AppRoutingModule { }
