export const AppLevelConstants = {
	FILTERED_OPTIONS: ['Electronic', 'Beauty', 'Stationary', 'grocessary'],
	ITEM_LISTING: 'Item List',
	ADD_ITEM: 'Add Item',
	UPDATE_ITEM: 'Update Item',
	COLUMN_DATA_USER_LISTING: ['User Name', 'User Role', 'Email', 'Updated By', 'Created On', 'Action'],

	DELETE_POPUP_TITLE: 'Delete Item',
	DELETE_POPUP_MSG: 'Are you sure you want to delete the item ?',

	CANCEL_POPUP_TITLE: 'Confirmation',
	CANCEL_POPUP_MSG: 'Are you sure you want to continue without saving the details?',

	BUTTON_LABEL_YES: 'Yes',
	BUTTON_LABEL_NO: 'No',
	BUTTON_LABEL_SAVE: 'SAVE',

};
