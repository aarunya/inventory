import { HttpHeaders } from '@angular/common/http';
import { GlobalVariable } from 'src/app/shared/configurations/global-variable';
import { AppLevelConstants } from './app-level-constants';

/**
 * Routes for API call
 */
const globalvar = GlobalVariable;
const appLevelConstants = AppLevelConstants;
export const ROUTE_API_URL = {
};

/**
 * Headers for POST API call
 */
export let HTTP_OPTIONS = {
	headers: new HttpHeaders({
		'Content-Type': 'application/json',
		'Access-Control-Allow-Origin': '*',
		'Access-Control-Allow-Methods': 'GET',
	}),
};
