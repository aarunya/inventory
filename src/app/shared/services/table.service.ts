import { Injectable, QueryList } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

@Injectable()
export class TableService {

	constructor() { }

	public configureSortAndPagination(
		sort: QueryList<MatSort>,
		paginator: QueryList<MatPaginator>,
		dataSource: MatTableDataSource<any>): void {
		if (sort && paginator) {
			sort.changes.subscribe(() => {
				dataSource.sort = sort.last;
			});
			paginator.changes.subscribe(() => {
				dataSource.paginator = paginator.last;
			});
			dataSource.sortingDataAccessor = this.sortingDataAccessor;
		}
	}

	sortingDataAccessor(item: any, path: string): any {
		let result = path.split('.').reduce(
			(accumulator: any, key: string) => {
				return accumulator ? accumulator[key] : undefined;
			},
			item);
		if (path.toLowerCase() === 'lastlogindate') {
			result = new Date(item.lastLoginDate);
		}
		return typeof result === 'string' ? result.toLowerCase() : result;
	}

}
