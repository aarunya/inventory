import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root',
})
export class PopupService {

	constructor() { }
	popupMsg;
	popupTitle;
	popupConfirmationMsg;
	popupConfirmationTitle;
	popUpAction;
	setPopupMsg(popupMsg) {
		this.popupMsg = popupMsg;
	}
	setPopupTitle(popupTitle) {
		this.popupTitle = popupTitle;
	}
	setConfirmationPopupMsg(popupConfirmationMsg) {
		this.popupConfirmationMsg = popupConfirmationMsg;
	}
	setConfirmationPopupTitle(popupConfirmationTitle) {
		this.popupConfirmationTitle = popupConfirmationTitle;
	}
	setPopUpAction(popUpAction) {
		this.popUpAction = popUpAction;
	}
}
