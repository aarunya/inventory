import { Component, Input, OnInit } from '@angular/core';
import { BreadcrumbElement } from '../../models/breadcrumb.model';

@Component({
	selector: 'app-breadcrumb',
	templateUrl: './breadcrumb.component.html',
	styleUrls: ['./breadcrumb.component.scss'],
})
export class BreadcrumbComponent implements OnInit {
	@Input() backLink: BreadcrumbElement[];
	constructor() { }

	ngOnInit() {
	}

}
