import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';
import { AppLevelConstants } from '../../configurations/app-level-constants';
import { PopupService } from '../../services/popup.service';
// tslint:disable-next-line: interface-name
export interface DialogData {
	isDelete: boolean;
}
@Component({
	selector: 'app-confirmation-popup',
	templateUrl: './confirmation-popup.component.html',
	styleUrls: ['./confirmation-popup.component.scss'],
})

export class ConfirmationPopupComponent implements OnInit {

	requestObject = {
		user_role: '',
		model_id: '',
	};
	LABEL_NO = AppLevelConstants.BUTTON_LABEL_NO;
	LABEL_YES = AppLevelConstants.BUTTON_LABEL_YES;
	action = '';
	popUpResponse: boolean;
	requestBody: {};

	constructor(
		public dialogRef: MatDialogRef<ConfirmationPopupComponent>,
		public popUpService: PopupService,
		public dialog: MatDialog,
	) { }

	ngOnInit() { }

	popupclose() {
		this.popUpResponse = false;
		this.dialogRef.close(this.popUpResponse);
		this.dialogRef.close(true);
	}
	back() {
		this.dialogRef.close(true);
	}
	saveChanges() {
		this.action = this.popUpService.popUpAction;
		this.dialogRef.close(true);
	}

}
