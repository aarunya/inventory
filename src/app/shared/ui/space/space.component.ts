import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
	selector: 'app-space',
	templateUrl: './space.component.html',
	styleUrls: ['./space.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SpaceComponent {

	@Input() space = 10;

	constructor() { }

}
