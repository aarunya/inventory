import { ValidatorFn, Validators } from '@angular/forms';
import { validateNumeric } from './numeric.validator';

export class AppValidators extends Validators {
	static numeric: ValidatorFn = validateNumeric;
}