import { LayoutModule } from '@angular/cdk/layout';
import { CommonModule, DatePipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
	MatAutocompleteModule,
	MatButtonModule,
	MatCardModule,
	MatDatepickerModule,
	MatDialogModule,
	MatFormFieldModule,
	MatIconModule,
	MatInputModule,
	MatMenuModule,
	MatNativeDateModule,
	MatPaginatorModule,
	MatSelectModule,
	MatSidenavModule,
	MatSortModule,
	MatTableModule,
	MatToolbarModule,
	MatTooltipModule,
} from '@angular/material';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { ToastrModule } from 'ngx-toastr';
import { InMemoryDataService } from '../inventory-management/shared/services/in-memory-data.service';
import { PopupService } from './services/popup.service';
import { TableService } from './services/table.service';
import { BreadcrumbComponent } from './ui/breadcrumb/breadcrumb.component';
import { ButtonComponent } from './ui/button/button.component';
import { ConfirmationPopupComponent } from './ui/confirmation-popup/confirmation-popup.component';
import { SpaceComponent } from './ui/space/space.component';

@NgModule({
	schemas: [
		CUSTOM_ELEMENTS_SCHEMA,
	],
	imports: [
		CommonModule,
		BrowserModule,
		BrowserAnimationsModule,
		FormsModule,
		ReactiveFormsModule,
		MatSidenavModule,
		MatIconModule,
		LayoutModule,
		MatToolbarModule,
		MatButtonModule,
		MatMenuModule,
		MatSidenavModule,
		MatButtonModule,
		RouterModule,
		MatDatepickerModule,
		MatDialogModule,
		MatSelectModule,
		MatInputModule,
		MatPaginatorModule,
		MatTableModule,
		MatFormFieldModule,
		MatAutocompleteModule,
		MatCardModule,
		HttpClientTestingModule,
		HttpClientModule,
		InMemoryWebApiModule.forRoot(InMemoryDataService),
		MatNativeDateModule,
		MatTooltipModule,
		ToastrModule.forRoot({
			timeOut: 10000,
			positionClass: 'toast-bottom-right',
			titleClass: 'toast-title',
			preventDuplicates: true,
			extendedTimeOut: 4000,
			closeButton: true,
			enableHtml: true,
		}),
		MatSortModule,
		MatProgressSpinnerModule,

	],
	declarations: [
		ConfirmationPopupComponent,
		ButtonComponent,
		BreadcrumbComponent,
		SpaceComponent],
	entryComponents: [ConfirmationPopupComponent],
	providers: [
		DatePipe,
		TableService,
		PopupService,
	],
	exports: [
		FormsModule,
		MatSidenavModule,
		MatIconModule,
		LayoutModule,
		MatToolbarModule,
		MatButtonModule,
		MatMenuModule,
		MatSidenavModule,
		BrowserModule,
		BrowserAnimationsModule,
		MatButtonModule,
		MatDialogModule,
		MatSelectModule,
		MatInputModule,
		MatPaginatorModule,
		MatTableModule,
		MatCardModule,
		MatFormFieldModule,
		MatAutocompleteModule,
		MatTooltipModule,
		HttpClientModule,
		InMemoryWebApiModule,
		MatDatepickerModule,
		MatNativeDateModule,
		MatSortModule,
		HttpClientTestingModule,
		MatProgressSpinnerModule,
		ReactiveFormsModule,
		ToastrModule,
		ButtonComponent,
		ConfirmationPopupComponent,
		BreadcrumbComponent,
		SpaceComponent,
	],
})
export class SharedModule { }
