export interface BreadcrumbElement {
	path?: string;
	label: string;
}