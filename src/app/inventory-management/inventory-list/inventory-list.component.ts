import { AfterViewInit, Component, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { MatDialog, MatSort, ProgressSpinnerMode, ThemePalette } from '@angular/material';
import { MatPaginator } from '@angular/material';
import { MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppLevelConstants } from 'src/app/shared/configurations/app-level-constants';
import { PopupService } from 'src/app/shared/services/popup.service';
import { ConfirmationPopupComponent } from 'src/app/shared/ui/confirmation-popup/confirmation-popup.component';
import { InventoryItemElements } from '../shared/models/inventory-item.model';
import * as fromStore from '../shared/store';

/**
 * @title Table with pagination
 */
@Component({
	selector: 'app-inventory-list',
	templateUrl: './inventory-list.component.html',
	styleUrls: ['./inventory-list.component.scss'],
})
export class InventoryListComponent implements AfterViewInit, OnInit {
	@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
	@ViewChildren(MatSort) sort: MatSort;

	displayedColumns: string[] = ['action', 'name', 'price', 'quantity', 'total'];
	public listData: InventoryItemElements[];
	public dataSource = new MatTableDataSource<InventoryItemElements>([]);
	constructor(
		public dialog: MatDialog,
		public popUpService: PopupService,
		private store: Store<fromStore.IState>,
		public router: Router,
	) { }

	color: ThemePalette = 'primary';
	mode: ProgressSpinnerMode = 'indeterminate';
	value = 10;
	public isLoading = false;
	ngOnInit() {
		this.store.select(fromStore.getAllInventory)
			.subscribe((data) => {
				this.isLoading = true;
				setTimeout(() => {
					this.listData = data.map((item) => ({ ...item, itemTotal: item.itemPrice * item.itemQuantity }));
					this.dataSource = new MatTableDataSource<InventoryItemElements>(this.listData);
					this.dataSource.paginator = this.paginator;
					this.dataSource.sort = this.sort;
					this.isLoading = false;
				}, 1000);
			});
	}
	openDeleteDialog(id: number) {
		this.popUpService.setPopupTitle(AppLevelConstants.DELETE_POPUP_TITLE);
		this.popUpService.setConfirmationPopupMsg(AppLevelConstants.DELETE_POPUP_MSG);
		const deleteResponse = this.dialog.open(ConfirmationPopupComponent, {});
		deleteResponse.afterClosed().subscribe((isDelete) => {
			if (isDelete) {
				this.isLoading = true;
				this.store.dispatch(new fromStore.DeleteInventory(id));
			}
		});
	}

	openEditRoute(id: string) {
		this.router.navigate(['update-item/' + id]);
	}
	ngAfterViewInit() {
		this.dataSource.paginator = this.paginator;
		this.dataSource.sort = this.sort;
	}
}
