import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogRef, MatIconModule,
	MatPaginatorModule, MatSortModule, MatTableModule, MatTooltipModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';

import { InventoryListComponent } from './inventory-list.component';

class MockStore {
	public dispatch(obj) {
	}

	public select(obj) {
		return of({});
	}
}
export class MatDialogMock {
	open() {
		return {
			afterClosed: () => of({ name: 'some object' }),
		};
	}
}
describe('InventoryListComponent', () => {
	let component: InventoryListComponent;
	let fixture: ComponentFixture<InventoryListComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			schemas: [
				CUSTOM_ELEMENTS_SCHEMA,
			],
			imports: [RouterModule, RouterTestingModule, MatTableModule, MatTooltipModule, MatIconModule, MatPaginatorModule, MatSortModule],
			declarations: [InventoryListComponent],
			providers: [{ provide: MatDialog, useClass: MatDialogMock }, { provide: Store, useClass: MockStore }],
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(InventoryListComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
