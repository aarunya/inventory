import { Component, CUSTOM_ELEMENTS_SCHEMA, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppLevelConstants } from 'src/app/shared/configurations/app-level-constants';
import { BreadcrumbElement } from 'src/app/shared/models/breadcrumb.model';
import { PopupService } from 'src/app/shared/services/popup.service';
import { ConfirmationPopupComponent } from 'src/app/shared/ui/confirmation-popup/confirmation-popup.component';
import { AppValidators } from 'src/app/shared/validators/custom.validators';
import { InventoryItemElements } from '../shared/models/inventory-item.model';
import * as fromStore from '../shared/store';

@Component({
	selector: 'app-add-inventory',
	templateUrl: './add-inventory.component.html',
	styleUrls: ['./add-inventory.component.scss'],
})
export class AddInventoryComponent implements OnInit {
	public form: FormGroup;
	LABELS = AppLevelConstants;
	filteredOptions: Observable<string[]>;
	options: string[] = AppLevelConstants.FILTERED_OPTIONS;
	public backLink: BreadcrumbElement[] = [
		{ label: AppLevelConstants.ITEM_LISTING, path: '/listing' },
		{ label: AppLevelConstants.ADD_ITEM }];
	constructor(
		public dialog: MatDialog,
		public popUpService: PopupService,
		private router: Router,
		private store: Store<fromStore.IState>,
	) { }

	ngOnInit() {
		this.buildForm();
	}

	private buildForm(): void {
		this.form = new FormGroup({
			itemName: new FormControl('', [AppValidators.required]),
			itemPrice: new FormControl('', [AppValidators.required, AppValidators.numeric]),
			itemQuantity: new FormControl('', [AppValidators.required, AppValidators.numeric]),
			itemDescription: new FormControl(''),
		});
	}

	public onSubmit(): void {
		if (this.form.valid) {
			const newItem = Object.assign({}, this.form.value) as InventoryItemElements;
			this.store.dispatch(new fromStore.AddInventory(newItem));
			this.router.navigate(['/listing']);
		}
	}
	public back(): void {
		this.popUpService.setPopupTitle(AppLevelConstants.CANCEL_POPUP_TITLE);
		this.popUpService.setConfirmationPopupMsg(AppLevelConstants.CANCEL_POPUP_MSG);
		this.popUpService.setPopUpAction('cancelModel');
		const deleteResponse = this.dialog.open(ConfirmationPopupComponent, { disableClose: true });
		deleteResponse.afterClosed().subscribe((isCancel) => {
			if (isCancel) {
				this.router.navigate(['/listing']);
			}
		});
	}
}
