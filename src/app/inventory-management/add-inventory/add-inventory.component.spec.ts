import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { InventoryItemElements } from '../shared/models/inventory-item.model';

import { AddInventoryComponent } from './add-inventory.component';

class MockStore {
	public dispatch(obj) {
	}

	public select(obj) {
		return of({});
	}
}
export class MatDialogMock {
	open() {
		return {
			afterClosed: () => of({ name: 'some object' }),
		};
	}
}
describe('AddInventoryComponent', () => {
	let component: AddInventoryComponent;
	let fixture: ComponentFixture<AddInventoryComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			schemas: [
				CUSTOM_ELEMENTS_SCHEMA,
			],
			imports: [ReactiveFormsModule, RouterModule, RouterTestingModule],
			providers: [{ provide: MatDialog, useClass: MatDialogMock }, { provide: Store, useClass: MockStore }],
			declarations: [AddInventoryComponent],
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(AddInventoryComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('should display the title as Add Item', () => {
		expect(component.LABELS.ADD_ITEM).toContain('Add Item');
	});

	it('should call editSelectedRow', () => {
		const newItem: InventoryItemElements = {
			itemId: 2,
			itemName: 'Test',
			itemCategory: 'Grocessary',
			itemPrice: 43,
			itemQuantity: 5,
		};
		spyOn(AddInventoryComponent.prototype, 'onSubmit').and.callThrough();
		component.onSubmit();
		expect(AddInventoryComponent.prototype.onSubmit).toHaveBeenCalled();
	}, 2500000);
});
