import { InventoryItemElements } from '../models/inventory-item.model';

export const INVENTORY_DATA: InventoryItemElements[] = [
	{
		itemId: 1,
		itemName: 'Suger',
		itemCategory: 'Grocessary',
		itemPrice: 20,
		itemQuantity: 3,
	},
	{
		itemId: 2,
		itemName: 'Vegetable Oil',
		itemCategory: 'Grocessary',
		itemPrice: 43,
		itemQuantity: 5,
	},
	{
		itemId: 3,
		itemName: 'face Wash',
		itemCategory: 'Beauty and Care',
		itemPrice: 64,
		itemQuantity: 6,
	},
	{
		itemId: 4,
		itemName: 'Sanitizer',
		itemCategory: 'Beauty and Care',
		itemPrice: 10,
		itemQuantity: 90,
	},
];