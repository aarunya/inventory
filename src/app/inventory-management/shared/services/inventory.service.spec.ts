import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { async, TestBed } from '@angular/core/testing';
import * as inventoryService from '../services/inventory.service';
import { InventoryService } from './inventory.service';

describe('InventoryService', () => {
	let service: inventoryService.InventoryService;
	let httpMock: HttpTestingController;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [HttpClientTestingModule, HttpClientModule],
			providers: [inventoryService.InventoryService],
		})
			.compileComponents();
		service = TestBed.get(inventoryService.InventoryService);
		httpMock = TestBed.get(HttpTestingController);
	}));
	afterEach(() => {
		httpMock.verify();
	});
});
