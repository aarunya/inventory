import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { InMemoryDataService } from './in-memory-data.service';

describe('InMemoryDataService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [HttpClient, InMemoryDataService],
		}),
		TestBed.configureTestingModule({});
	});

	it('should be created', () => {
		const service: InMemoryDataService = TestBed.get(InMemoryDataService);
		expect(service).toBeTruthy();
	});
});
