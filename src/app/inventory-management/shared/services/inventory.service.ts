import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { InventoryItemElements } from '../models/inventory-item.model';

@Injectable({
	providedIn: 'root',
})
export class InventoryService {
	// public inventoryListData =
	public SERVER_URL = 'api/inventoryData';
	constructor(
		private http: HttpClient,
	) { }

	public getInventoryList() {
		return this.http.get<InventoryItemElements[]>(this.SERVER_URL)
			.pipe(
				catchError((error) => {
					this.handleError(error);
					return throwError(error);
				}));
	}

	public getInventoryItem(id) {
		return this.http.get(`${this.SERVER_URL}/${id}`)
			.pipe(
				catchError((error) => {
					this.handleError(error);
					return throwError(error);
				}));
	}
	public createInventoryItem(item: InventoryItemElements) {
		return this.http.post(`${this.SERVER_URL}`, item);
	}

	public deleteInventoryItem(id: number) {
		return this.http.delete(`${this.SERVER_URL}/'1'`);
	}
	public updateInventoryItem(item: InventoryItemElements) {
		return this.http.put(`${this.SERVER_URL}/${item.itemId}`, item);
	}

	handleError(error) {

		let errorMessage;
		if (error.error instanceof ErrorEvent) {
			errorMessage = error.error.message;
		} else {
			errorMessage = error.message;
		}
		return throwError(errorMessage);

	}
}
