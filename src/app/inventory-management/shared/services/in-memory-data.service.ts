import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { INVENTORY_DATA } from '../mock-up/item-list.mock';
import { InventoryItemElements } from '../models/inventory-item.model';

@Injectable({
	providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
	createDb() {
		const inventoryData: InventoryItemElements[] = INVENTORY_DATA;
		return { inventoryData };
	}

	// Overrides the genId method to ensure that a hero always has an id.
	// If the items array is empty,
	// the method below returns the initial number (11).
	// if the items array is not empty, the method below returns the highest
	// hero id + 1.
	genId(items): number {
		return items.length > 0 ? Math.max(...items.map((item) => item.id)) + 1 : 11;
	}
}