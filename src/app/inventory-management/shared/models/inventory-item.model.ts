// tslint:disable-next-line: interface-name
export interface InventoryItemElements {
	itemId?: number;
	itemName: string;
	itemPrice: number;
	itemCategory: string;
	itemQuantity: number;
	itemTotal?: number;
	itemDescription?: number;
	addedDate?: Date;
	updatedDate?: Date;
}