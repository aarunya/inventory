import { INVENTORY_DATA } from '../../mock-up/item-list.mock';
import { InventoryItemElements } from '../../models/inventory-item.model';
import * as fromInventory from '../actions/inventory.action';

// tslint:disable-next-line: interface-name
export interface InventoryitemDataState {
	itemData: InventoryItemElements[];
	selectedItem: {};
}

export const initialState: InventoryitemDataState = {
	itemData: INVENTORY_DATA,
	selectedItem: {},
};

export function reducer(
	state = initialState,
	action: fromInventory.InventoryAction,
): InventoryitemDataState {
	switch (action.type) {
		case fromInventory.LOAD_INVENTORY: {
			return {
				...state,
				itemData: INVENTORY_DATA,
			};
		}
		case fromInventory.GET_INVENTORY: {
			const iteamId = action.payload;
			const selectedItem = state.itemData.filter((item) => item.itemId === iteamId);
			return {
				...state,
				selectedItem,
			};
		}

		case fromInventory.ADD_INVENTORY: {
			const data = [...state.itemData];
			data.unshift(action.payload);
			return {
				...state,
				itemData: data,
			};
		}

		case fromInventory.UPDATE_INVENTORY: {
			const itemData = [...state.itemData];
			itemData[action.payload.id] = action.payload.item;
			return {
				...state,
				itemData,
			};
		}

		case fromInventory.DELETE_INVENTORY: {
			const itemData = [...state.itemData];
			itemData.splice(1, 1);
			return {
				...state,
				itemData,
			};
		}
		case fromInventory.CLEAR_INVENTORY_STATE: {
			return {
				...state,
				itemData: [],
			};
		}
	}
	return state;
}

export const getInventory = (state: InventoryitemDataState) => state.itemData;
export const getSelectedInventory = (state: InventoryitemDataState) => state.selectedItem;
