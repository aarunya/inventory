import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';
import * as fromInventory from '../reducers/inventory.reducer';

export interface IState {
	inventorys: fromInventory.InventoryitemDataState;
}

export const reducers: ActionReducerMap<IState> = {
	inventorys: fromInventory.reducer,
};

export const getInventoryState = createFeatureSelector<IState>('inventory');
