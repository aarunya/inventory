import { createSelector } from '@ngrx/store';

import * as fromRoot from '../../store';
import * as fromFeature from '../reducers';
import * as fromInventory from '../reducers/inventory.reducer';

// inventorys state
export const GetInventoryState = createSelector(
	fromFeature.getInventoryState,
	(state: fromFeature.IState) => state.inventorys,
);

export const getAllInventory = createSelector(
	GetInventoryState,
	fromInventory.getInventory,
);

export const getSelectedItem = (id) => createSelector(
	getAllInventory,
	(data) => {
		return data.find((item, index) => index === Number(id));
	},
);
