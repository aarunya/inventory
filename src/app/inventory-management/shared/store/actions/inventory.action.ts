import { Action } from '@ngrx/store';

// load inventorys
export const LOAD_INVENTORY = 'LOAD_INVENTORY';
export const ADD_INVENTORY = 'ADD_INVENTORY';
export const DELETE_INVENTORY = 'DELETE_INVENTORY';
export const GET_INVENTORY = 'GET_INVENTORY';
export const CLEAR_INVENTORY_STATE = 'CLEAR_INVENTORY_STATE';
export const UPDATE_INVENTORY = 'UPDATE_INVENTORY';

export class LoadInventory implements Action {
	readonly type = LOAD_INVENTORY;
}

export class AddInventory implements Action {
	readonly type = ADD_INVENTORY;
	constructor(public payload: any) { }
}

export class GetInventory implements Action {
	readonly type = GET_INVENTORY;
	constructor(public payload: any) { }
}

export class UpdateInventory implements Action {
	readonly type = UPDATE_INVENTORY;
	constructor(public payload: any) { }
}
export class DeleteInventory implements Action {
	readonly type = DELETE_INVENTORY;
	constructor(public payload: any) { }
}

export class ClearInventoryState implements Action {
	readonly type = CLEAR_INVENTORY_STATE;
}

// action types
export type InventoryAction = LoadInventory | AddInventory | GetInventory | ClearInventoryState | UpdateInventory | DeleteInventory;
