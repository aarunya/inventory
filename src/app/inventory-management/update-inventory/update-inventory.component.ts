import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog, ProgressSpinnerMode, ThemePalette } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { ToastrService } from 'ngx-toastr';
import { take } from 'rxjs/operators';
import { AppLevelConstants } from 'src/app/shared/configurations/app-level-constants';
import { BreadcrumbElement } from 'src/app/shared/models/breadcrumb.model';
import { PopupService } from 'src/app/shared/services/popup.service';
import { ConfirmationPopupComponent } from 'src/app/shared/ui/confirmation-popup/confirmation-popup.component';
import { AppValidators } from 'src/app/shared/validators/custom.validators';
import { InventoryItemElements } from '../shared/models/inventory-item.model';
import * as fromStore from '../shared/store';

@Component({
	selector: 'app-update-inventory',
	templateUrl: './update-inventory.component.html',
	styleUrls: ['./update-inventory.component.scss'],
})
export class UpdateInventoryComponent implements OnInit {
	public itemDetails: InventoryItemElements;
	public LABELS = AppLevelConstants;
	public form: FormGroup;
	public isLoading = true;
	public selectedId: number;
	public backLink: BreadcrumbElement[] = [
		{ label: AppLevelConstants.ITEM_LISTING, path: '/listing' },
		{ label: AppLevelConstants.UPDATE_ITEM }];
	color: ThemePalette = 'primary';
	mode: ProgressSpinnerMode = 'indeterminate';
	value = 10;
	constructor(
		public dialog: MatDialog,
		public popUpService: PopupService,
		private router: Router,
		private store: Store<fromStore.IState>,
		private activeRoute: ActivatedRoute,
	) { }

	ngOnInit() {
		this.activeRoute.params.subscribe((params) => {
			this.isLoading = true;
			this.selectedId = Number(params.item);
			this.store.select(fromStore.getSelectedItem(params.item)).pipe(
				take(1))
				.subscribe((response) => {
					this.itemDetails = response;
					this.buildForm(response);
				});
		});
	}

	private buildForm(itemDetails): void {
		this.form = new FormGroup({
			itemName: new FormControl(itemDetails.itemName, [AppValidators.required]),
			itemPrice: new FormControl(itemDetails.itemPrice, [AppValidators.required, AppValidators.numeric]),
			itemQuantity: new FormControl(itemDetails.itemQuantity, [AppValidators.required, AppValidators.numeric]),
			itemDescription: new FormControl(itemDetails.itemDescription),
		});
	}

	public onSubmit(): void {
		if (this.form.valid) {
			const updatedItem = Object.assign({}, this.form.value) as InventoryItemElements;
			this.store.dispatch(new fromStore.UpdateInventory({
				item: updatedItem, id: this.selectedId,
			}));
			this.router.navigate(['/listing']);
		}
	}
	public cancelPopup(): void {
		this.popUpService.setPopupTitle(AppLevelConstants.CANCEL_POPUP_TITLE);
		this.popUpService.setConfirmationPopupMsg(AppLevelConstants.CANCEL_POPUP_MSG);
		this.popUpService.setPopUpAction('cancelModel');
		const deleteResponse = this.dialog.open(ConfirmationPopupComponent, { disableClose: true });
		deleteResponse.afterClosed().subscribe((isCancel) => {
			if (isCancel) {
				this.router.navigate(['/listing']);
			}
		});
	}
}
