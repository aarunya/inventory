import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';

import { UpdateInventoryComponent } from './update-inventory.component';

class MockStore {
	public dispatch(obj) {
	}

	public select(obj) {
		return of({});
	}
}
export class MatDialogMock {
	open() {
		return {
			afterClosed: () => of({ name: 'some object' }),
		};
	}
}
describe('UpdateInventoryComponent', () => {
	let component: UpdateInventoryComponent;
	let fixture: ComponentFixture<UpdateInventoryComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			schemas: [
				CUSTOM_ELEMENTS_SCHEMA,
			],
			imports: [ReactiveFormsModule, RouterModule, RouterTestingModule],
			declarations: [UpdateInventoryComponent],
			providers: [{ provide: MatDialog, useClass: MatDialogMock }, { provide: Store, useClass: MockStore }],
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(UpdateInventoryComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('should call ngOnInit', () => {

		spyOn(UpdateInventoryComponent.prototype, 'ngOnInit').and.callThrough();
		component.ngOnInit();
		expect(UpdateInventoryComponent.prototype.ngOnInit).toHaveBeenCalled();
	}, 2500000);

	it('should display the title as Update Item', () => {
		expect(component.LABELS.UPDATE_ITEM).toContain('Update Item');
	});
});
