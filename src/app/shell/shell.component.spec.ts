import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatDialogModule, MatDialogRef,
	MatIconModule, MatMenuModule, MatTableModule, MatToolbarModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { BreadcrumbComponent } from '../shared/ui/breadcrumb/breadcrumb.component';
import { ButtonComponent } from '../shared/ui/button/button.component';
import { HeaderComponent } from './header/header.component';

import { ShellComponent } from './shell.component';

describe('ShellComponent', () => {
	let component: ShellComponent;
	let fixture: ComponentFixture<ShellComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			schemas: [
				CUSTOM_ELEMENTS_SCHEMA,
			],
			imports: [MatIconModule, MatMenuModule, MatToolbarModule, RouterModule,
				MatButtonModule, MatTableModule, ReactiveFormsModule, FormsModule,
				RouterTestingModule, MatDialogModule, HttpClientModule],
			declarations: [
				ShellComponent, ButtonComponent, HeaderComponent, BreadcrumbComponent,
			],
			providers: [],
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ShellComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

});
