import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { AddInventoryComponent } from '../inventory-management/add-inventory/add-inventory.component';
import { InventoryListComponent } from '../inventory-management/inventory-list/inventory-list.component';
import { reducers } from '../inventory-management/shared/store';
import { UpdateInventoryComponent } from '../inventory-management/update-inventory/update-inventory.component';
import { SharedModule } from '../shared/shared.module';
import { HeaderComponent } from './header/header.component';
import { ShellComponent } from './shell.component';

export const ROUTES: Routes = [
	{
		path: '',
		component: ShellComponent,
		children: [
			{ path: '', pathMatch: 'full', redirectTo: 'listing' },
			{
				path: 'listing', component: InventoryListComponent,
			},
			{ path: 'update-item/:item', component: UpdateInventoryComponent },
			{
				path: 'add-item', component: AddInventoryComponent,
			},
		],
	},
];

@NgModule({
	schemas: [
		CUSTOM_ELEMENTS_SCHEMA,
	],
	declarations: [
		ShellComponent,
		HeaderComponent,
		AddInventoryComponent,
		UpdateInventoryComponent,
		InventoryListComponent,

	],
	imports: [
		CommonModule,
		RouterModule.forChild(ROUTES),
		StoreModule.forFeature('inventory', reducers),
		SharedModule,
	],
	exports: [
		HeaderComponent,
	],
})
export class ShellModule { }
