import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderComponent } from './header.component';

describe('HeaderComponent', () => {
	let component: HeaderComponent;
	let fixture: ComponentFixture<HeaderComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			schemas: [
				CUSTOM_ELEMENTS_SCHEMA,
			],
			declarations: [HeaderComponent],
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(HeaderComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('should call ngOnInit', () => {

		spyOn(HeaderComponent.prototype, 'ngOnInit').and.callThrough();
		component.ngOnInit();
		expect(HeaderComponent.prototype.ngOnInit).toHaveBeenCalled();
	}, 2500000);
});
