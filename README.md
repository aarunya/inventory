# Intern + Angular

This is a Inventory management project to demonstrate using Intern with Angular 8.  
It is basiacally a CRUD on inventory of general shop.

## Get started

### Prerequisites
To start working with project you need to have node in your project
1.  Node Versoin -12.13.0
2.  Visual studio code (Code Editor)

### Clone the repo

```shell
git clone https://aarunya@bitbucket.org/aarunya/inventory.git
cd inventory
```

### Install npm packages

Install the `npm` packages described in the `package.json` and verify that it works:

```shell
npm install
npm start
```

The `npm start` command builds (compiles TypeScript and copies assets) the application into `dist/`, watches for changes to the source files, and runs `lite-server` on port `3000`.

Shut it down manually with `Ctrl-C`.

#### npm scripts

These are the most useful commands defined in `package.json`:

* `npm start` - runs the TypeScript compiler, asset copier, and a server at the same time, all three in "watch mode".
* `npm run build` - runs the TypeScript compiler and asset copier once.
* `npm run lint` - runs `tslint` on the project files.

These are the test-related scripts:

* `npm test` - builds the application and runs Intern tests (both unit and functional) one time.
